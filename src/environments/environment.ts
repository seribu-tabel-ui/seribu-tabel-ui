/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  env_name: 'DEV',
  is_demo: true,
  // backend_url: 'http://localhost:7777',
  backend_url: 'http://68.183.186.212:7777',
  static_url: 'http://localhost:4200/seributabel',
  header_bearer: 'Bearer ',
  authorization: 'Authorization',
  accept: 'Accept',
  date_format: 'DD/MM/YYYY',
  content_type: 'Content-Type',
  json_content: 'application/json',
};
