/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
export const environment = {
  production: true,
  env_name: 'PROD',
  is_demo: false,
  backend_url: 'http://10.254.128.214:7777',
  static_url: 'http://10.254.128.214:4200',
  header_bearer: 'Bearer ',
  authorization: 'Authorization ',
  accept: 'Accept',
  date_format: 'DD/MM/YYYY',
  content_type: 'Content-Type',
  json_content: 'application/json',
};
