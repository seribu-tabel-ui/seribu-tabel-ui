import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import {
  NbAlertModule,
  NbButtonModule,
  NbCardModule,
  NbCheckboxModule,
  NbInputModule,
  NbLayoutModule,
  NbSpinnerModule,
} from '@nebular/theme';
import { AuthComponent } from './components/auth.component';
import { NbAuthBlockComponent } from './components/auth-block/auth-block.component';
import { NgxLoginComponent } from './components/login/login.component';
// import { NbLogoutComponent } from './logout/logout/logout.component';
import { routes } from './auth.routes';
import { CookiesService } from '../shared/services/cookies.service';
import { AuthService } from '../shared/services/auth.service';
import { ImageService } from '../shared/services/image.service';
import { ActivityService } from '../shared/services/activity.service';

@NgModule({
  imports: [
    CommonModule,
    NbLayoutModule,
    NbCardModule,
    NbCheckboxModule,
    NbAlertModule,
    NbInputModule,
    NbButtonModule,
    RouterModule.forChild(routes),
    FormsModule,
    HttpClientModule,
    NbSpinnerModule,
  ],
  declarations: [
    AuthComponent,
    NbAuthBlockComponent,
    NgxLoginComponent,
    // NbLogoutComponent,
  ],
  exports: [
    AuthComponent,
    NbAuthBlockComponent,
    NgxLoginComponent,
    // NbLogoutComponent,
  ],
  providers: [
    CookiesService,
    AuthService,
    ImageService,
    ActivityService,
  ],
})
export class AuthModule {

}
