import { Component, OnInit, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { LoginRequest } from '../../../models/login-request';
import { AuthService } from '../../../shared/services/auth.service';
import { CookiesService } from '../../../shared/services/cookies.service';
import { NB_AUTH_OPTIONS, getDeepFromObject } from '@nebular/auth';
import { UserModel } from '../../../models/user-model';
import { environment } from '../../../../environments/environment';
import { ImageService } from '../../../shared/services/image.service';
import { ActivityService } from '../../../shared/services/activity.service';

@Component({
  selector: 'ngx-login',
  templateUrl: './login.component.html',
})
export class NgxLoginComponent implements OnInit {

  redirectDelay: number = 0;
  djpLogo: any;
  loadData: boolean = false;
  showMessages: any = {};
  errors: string[] = [];
  messages: string[] = [];
  loginRequest: LoginRequest = new LoginRequest();
  user: any = {};
  public response: any;

  constructor(
    private router: Router,
    private authService: AuthService,
    private cookiesService: CookiesService,
    private imageService: ImageService,
    private activityService: ActivityService,
    @Inject(NB_AUTH_OPTIONS) protected options = {},
  ) {
    this.showMessages = this.getConfigValue('forms.login.showMessages');
  }

  ngOnInit() {
    this.getImage();
  }

  getImage() {
    this.imageService.getLoginImage().subscribe(resp => {
      this.djpLogo = resp.djpLogo;
    });
  }

  onLogin(): void {
    this.loadData = true;
    this.authService.authenticate(this.loginRequest).subscribe(
      (result: any) => {
        const auth: string = result.headers.get(environment.authorization);
        const stats = result.status;
        const data: UserModel = result.body.result;
        if (stats === 200) {
          this.cookiesService.setCookie('user', JSON.stringify(data));
          this.cookiesService.setCookie('jwt', auth);
          this.successLogin();
        } else {
          this.errors[0] = 'Silakan cek kembali user ID dan password Anda';
        }
      },
      invalid => {
        this.loadData = false;
        if (invalid.status === 403) {
          this.errors.push('Silakan cek kembali user ID dan password Anda');
        } else {
          this.errors.push('Login gagal!');
        }
      });
  }

  successLogin() {
    this.activityService.saveLog('Login', null, null, null, null, null)
    .subscribe(
      response => {
        this.loadData = true;
      },
      error => {
        this.loadData = false;
        this.errors.push('Login gagal. Kesalahan sistem.');
      },
    );
    setTimeout(() => {
      return this.router.navigateByUrl('/pages/home');
    }, this.redirectDelay);
    this.loadData = false;
  }

  getConfigValue(key: string): any {
    return getDeepFromObject(this.options, key, null);
  }
}
