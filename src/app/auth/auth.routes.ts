
/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { Routes } from '@angular/router';

import { AuthComponent } from './components/auth.component';
import { NgxLoginComponent } from './components/login/login.component';
// import { NbLogoutComponent } from './logout/logout/logout.component';

export const routes: Routes = [
  {
    path: '',
    component: AuthComponent,
    children: [
      {
        path: '',
        redirectTo: 'login',
        pathMatch: 'full',
      },
      {
        path: 'login',
        component: NgxLoginComponent,
      },
      // {
      //   path: 'logout',
      //   component: NbLogoutComponent,
      // },
    ],
  },
];
