import { Injectable } from '@angular/core';

@Injectable()
export class CommonHelper {

  hasProp(obj, prop: string[]): boolean {
    return Object.prototype.hasOwnProperty.call(obj, ...prop);
  }

}
