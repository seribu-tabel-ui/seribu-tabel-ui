import { tap, catchError } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { environment} from '../../environments/environment';
import {
  HttpEvent,
  HttpInterceptor,
  HttpHandler,
  HttpRequest,
  HttpResponse,
  HttpErrorResponse,
} from '@angular/common/http';
import { AuthService } from '../shared/services/auth.service';

@Injectable()
export class CustomHttpInterceptor implements HttpInterceptor {
  constructor(private auth: AuthService) { }

  private token;

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (this.token === undefined) {
      this.token = '';
    }
    if (req.url === environment.backend_url + '/auth/login') {
      return next.handle(req).pipe(tap(
        (event) => {
          if (event instanceof HttpResponse) {
            this.token = event.headers.get('x-xsrf-token');
          }
        }));
    }

    const dupReq = req.clone({ headers: req.headers.set('x-xsrf-token', this.token) });
    return next.handle(dupReq).pipe(catchError((error, caught) => {
      this.handleAuthError(error);
      return of(error);
    }) as any);
  }

  private handleAuthError(err: HttpErrorResponse): Observable<any> {
    if (err.status === 412) {
      this.auth.errorLogout('Session Expired, ');
      return of(err.message);
    }
    throw of(err);
  }
}
