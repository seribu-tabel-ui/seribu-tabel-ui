import { PipeTransform, Pipe } from '@angular/core';
import { formatDate } from '@angular/common';

@Pipe({ name: 'date'})
export class DatePipe implements PipeTransform {
  transform(date: Date): string {
    return formatDate(date, 'dd-MM-yyyy', 'en');
  }

}
