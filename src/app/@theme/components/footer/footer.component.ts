import { Component } from '@angular/core';
import { environment } from '../../../../environments/environment';

@Component({
  selector: 'ngx-footer',
  styleUrls: ['./footer.component.scss'],
  template: `
    <span class="created-by">Copyrigth <b>@2019</b></span>
    <div class="demo-sign-container" *ngIf="showDemoSign">
      <span class="demo-sign"><b>DEMO</b></span>
    </div>
  `,
})
export class FooterComponent {
  showDemoSign: boolean = environment.is_demo;

}
