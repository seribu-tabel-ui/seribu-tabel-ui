import { Component, OnDestroy, OnInit } from '@angular/core';
import { NbMediaBreakpointsService, NbSidebarService, NbThemeService } from '@nebular/theme';

import { LayoutService } from '../../../@core/utils';
import { map, takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { CookiesService } from '../../../shared/services/cookies.service';
import { UserModel } from '../../../models/user-model';
import { environment } from '../../../../environments/environment';
import { Router } from '@angular/router';

@Component({
  selector: 'ngx-header',
  styleUrls: ['./header.component.scss'],
  templateUrl: './header.component.html',
})
export class HeaderComponent implements OnInit, OnDestroy {

  private destroy$: Subject<void> = new Subject<void>();
  userPictureOnly: boolean = false;
  user: UserModel;
  namaUser: string;
  showDemoSign: boolean = false;
  redirectDelay = 0;

  themes = [
    {
      value: 'default',
      name: 'Light',
    },
  ];

  currentTheme = 'default';

  userMenu = [{ title: 'Log out' }];

  constructor(private sidebarService: NbSidebarService,
    private themeService: NbThemeService,
    private layoutService: LayoutService,
    private breakpointService: NbMediaBreakpointsService,
    private cookies: CookiesService,
    private router: Router) {
  }

  ngOnInit() {
    if (environment.is_demo) {
      this.showDemoSign = true;
    }

    this.currentTheme = this.themeService.currentTheme;
    this.user = this.cookies.getCookie('user');
    const { xl } = this.breakpointService.getBreakpointsMap();
    this.themeService.onMediaQueryChange()
      .pipe(
        map(([, currentBreakpoint]) => currentBreakpoint.width < xl),
        takeUntil(this.destroy$),
      )
      .subscribe((isLessThanXl: boolean) => this.userPictureOnly = isLessThanXl);

    this.themeService.onThemeChange()
      .pipe(
        map(({ name }) => name),
        takeUntil(this.destroy$),
      )
      .subscribe(themeName => this.currentTheme = themeName);
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  toggleSidebar(): boolean {
    this.sidebarService.toggle(true, 'menu-sidebar');
    this.layoutService.changeLayoutSize();

    return false;
  }

  navigateHome() {
    setTimeout(() => {
      return this.router.navigateByUrl('/pages/home');
    }, this.redirectDelay);
    return false;
  }

  logout() {
    this.cookies.setLogoutCookie();
    setTimeout(() => {
      return this.router.navigateByUrl('/auth/login');
    }, this.redirectDelay);
  }
}
