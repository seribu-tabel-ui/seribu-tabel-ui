export class DownloadRequest {
  dataset: string;
  owner: string;
  fileName: string;
  fileType: string;
  releaseYear: number;
}
