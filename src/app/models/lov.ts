export class Lov {
  public value: string;
  public text: string;

  // constructor(text: string, value: string);
  constructor(text: string, value: string) {
    this.value = value;
    this.text = text;
  }
}
