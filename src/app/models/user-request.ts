export class UserRequest {
  id: string;
  username: string;
  name: string;
  nip: string;
  password: string;
  role: string;
  roleText: string;
  enabled: boolean;
  createdDate: any;

  constructor(username: string, name: string, nip: string, password: string
      , role: string, roleText: string, enabled: boolean, createdDate: any) {
    this.username = username;
    this.name = name;
    this.nip = nip;
    this.password = password;
    this.role = role;
    this.roleText = roleText;
    this.enabled = enabled;
    this.createdDate = createdDate;
  }
}
