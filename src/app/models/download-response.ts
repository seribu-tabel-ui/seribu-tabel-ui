export class DownloadResponse {
  dataset: string;
  owner: string;
  fileName: string;
  fileUri: string;
  fileType: string;
  releaseYear: number;
  description: string;
}
