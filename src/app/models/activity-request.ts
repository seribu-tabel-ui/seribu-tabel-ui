export class ActivityRequest {
  username: string;
  nip: string;
  authority: string;
  activity: string;
  dataset: string;
  subyekData: string;
  sourceData: string; // optional
  firstCharacter: string; // optional
  secondCharacter: string; // optional
  ipAddress: string;
  accessDate: Date;

  constructor(username: string,
    nip: string,
    authority: string,
    activity: string,
    dataset: string,
    subyekData: string,
    sourceData: string,
    firstCharacter: string,
    secondCharacter: string,
    ipAddress: string,
    accessDate: Date) {
      this.username = username;
      this.nip = nip;
      this.authority = authority;
      this.activity = activity,
      this.dataset = dataset;
      this.subyekData = subyekData;
      this.sourceData = sourceData; // optional
      this.firstCharacter = firstCharacter; // optional
      this.secondCharacter = secondCharacter; // optional
      this.ipAddress = ipAddress;
      this.accessDate = accessDate;
  }
}
