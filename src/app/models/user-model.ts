export interface UserModel {
  id: string;
  username: string;
  nip: string;
  nama: string;
  password: string;
  createdDate: Date;
  updatedDate: Date;
  role: string;
  enabled: boolean;
  authorities: [];
  accountNonExpired: boolean;
  accountNonLocked: boolean;
  credentialsNonExpired: boolean;
}
