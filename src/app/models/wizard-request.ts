export class WizardRequest {

  dataset: string;
  subyekData: string;
  sourceData: string;
  rowParam: string;
  columnParam: string;
  filterByYear: string[];

  constructor(dataset, subyekData, sourceData, rowParam, columnParam, filterByYear) {
    this.dataset = dataset;
    this.subyekData = subyekData;
    this.sourceData = sourceData;
    this.rowParam = rowParam;
    this.columnParam = columnParam;
    this.filterByYear = filterByYear;
  }
}
