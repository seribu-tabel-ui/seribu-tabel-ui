import { NbMenuItem } from '@nebular/theme';

export const MENU_ITEMS_SUPER: NbMenuItem[] = [
  {
    title: 'User Management',
    icon: 'people-outline',
    children: [
      {
        title: 'User Activity',
        link: '/pages/user/activity',
      },
      {
        title: 'User Registration',
        link: '/pages/user/registration',
      },
    ],
  },
  // {
  //   title: 'Master',
  //   icon: 'layout-outline',
  //   children: [
  //     {
  //       title: 'Kategori',
  //       link: '/pages/master/category',
  //     },
  //   ],
  // },
  {
    title: 'Data',
    icon: 'edit-2-outline',
    children: [
      {
        title: 'Daftar Tabel',
        link: '/pages/data/inquiry',
      },
      {
        title: 'Upload Data',
        link: '/pages/data/importupload',
      },
    ],
  },
];

export const MENU_ITEMS_ADMIN: NbMenuItem[] = [
  {
    title: 'User Management',
    icon: 'people-outline',
    children: [
      {
        title: 'User Activity',
        link: '/pages/user/activity',
      },
      {
        title: 'User Registration',
        link: '/pages/user/registration',
      },
    ],
  },
  {
    title: 'Data',
    icon: 'edit-2-outline',
    children: [
      {
        title: 'Daftar Tabel',
        link: '/pages/data/inquiry',
      },
      {
        title: 'Upload Data',
        link: '/pages/data/importupload',
      },
    ],
  },
];

export const MENU_ITEMS_USER: NbMenuItem[] = [
  {
    title: 'Data',
    icon: 'edit-2-outline',
    children: [
      {
        title: 'Daftar Tabel',
        link: '/pages/data/inquiry',
      },
      {
        title: 'Upload Data',
        link: '/pages/data/importupload',
      },
    ],
  },
];
