import { NgModule } from '@angular/core';
import { NbMenuModule } from '@nebular/theme';

import { ThemeModule } from '../@theme/theme.module';
import { PagesComponent } from './pages.component';
import { PagesRoutingModule } from './pages-routing.module';
import { HomeModule} from './home/home.module';
import { UserModule } from './user/user.module';
import { DataModule } from './data/data.module';

import { MiscellaneousModule } from './miscellaneous/miscellaneous.module';
import { CookiesService } from '../shared/services/cookies.service';

@NgModule({
  imports: [
    HomeModule,
    PagesRoutingModule,
    ThemeModule,
    NbMenuModule,
    MiscellaneousModule,
    UserModule,
    DataModule,
  ],
  declarations: [
    PagesComponent,
  ],
  providers: [
    CookiesService,
  ],
})
export class PagesModule {
}
