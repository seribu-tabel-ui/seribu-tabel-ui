import { Component } from '@angular/core';

import { MENU_ITEMS_ADMIN, MENU_ITEMS_USER } from './pages-menu';
import { MENU_ITEMS_SUPER } from './pages-menu';
import { CookiesService } from '../shared/services/cookies.service';
import { UserModel } from '../models/user-model';
import { NbMenuItem } from '@nebular/theme';

@Component({
  selector: 'ngx-pages',
  styleUrls: ['pages.component.scss'],
  template: `
    <ngx-one-column-layout>
      <nb-menu [items]="menu"></nb-menu>
      <router-outlet></router-outlet>
    </ngx-one-column-layout>
  `,
})
export class PagesComponent {
  loading: boolean = true;
  menu: NbMenuItem[];
  userLogedIn: UserModel;

  constructor(
    private cookiesService: CookiesService,
  ) {
    if (this.getUserLogingIn()[0].authority === 'ROLE_SUPER') {
      this.menu = MENU_ITEMS_SUPER;
    } else if (this.getUserLogingIn()[0].authority === 'ROLE_ADMIN') {
      this.menu = MENU_ITEMS_ADMIN;
    } else {
      this.menu = MENU_ITEMS_USER;
    }
  }

  getUserLogingIn(): any[] {
    this.userLogedIn = this.cookiesService.getCookie('user');
    return this.userLogedIn.authorities;
  }
}
