import { Component, OnInit } from '@angular/core';
import { ImportService } from '../../shared/services/import.service';
import { UploadService } from '../../shared/services/upload.service';

@Component({
  selector: 'ngx-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {

  totalDemografi: number = 0;
  totalKepatuhan: number = 0;
  totalPenerimaan: number = 0;
  totalSubdit: number = 0;
  totalPihakLain: number = 0;
  totalWhole: number = 0;

  constructor(
    private importService: ImportService,
    private uploadService: UploadService,
  ) { }

  ngOnInit() {
    this.importService.getTotalDemografi().subscribe(
      result => {
        this.totalDemografi = result.result;
      },
    );

    this.importService.getTotalKepatuhan().subscribe(
      result => {
        this.totalKepatuhan = result.result;
      },
    );

    this.importService.getTotalPenerimaan().subscribe(
      result => {
        this.totalPenerimaan = result.result;
      },
    );

    this.uploadService.getTotalSubdit().subscribe(
      result => {
        this.totalSubdit = result.result;
      },
    );

    this.uploadService.getTotalPihakLain().subscribe(
      result => {
        this.totalPihakLain = result.result;
      },
    );

    this.totalWhole = this.totalDemografi + this.totalKepatuhan
      + this.totalPenerimaan + this.totalSubdit + this.totalPihakLain;
  }

}
