import { NgModule } from '@angular/core';
import {
  NbButtonModule,
  NbCardModule,
} from '@nebular/theme';
import { ThemeModule } from '../../@theme/theme.module';
import { HomeComponent } from './home.component';
import { UploadService } from '../../shared/services/upload.service';
import { ImportService } from '../../shared/services/import.service';

@NgModule({
  imports: [ThemeModule
    , NbButtonModule
    , NbCardModule,
  ],
  declarations: [HomeComponent],
  providers: [
    UploadService,
    ImportService,
  ],
})
export class HomeModule { }
