import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { Lov } from '../../../models/lov';
import { FileUploader } from 'ng2-file-upload';
import { ImportService } from '../../../shared/services/import.service';
import { ActivityService } from '../../../shared/services/activity.service';

@Component({
  selector: 'ngx-import',
  templateUrl: './import.component.html',
  styleUrls: ['./import.component.scss'],
})
export class ImportComponent implements OnInit {

  subyekData: Lov[] = [];
  dataSource: Lov[] = [];
  countSuccess = 0;
  redirectDelay = 0;
  onProcess: boolean = false;
  errors: String[] = [];
  successShow: boolean = false;
  delayTime = 4500;
  importForm: FormGroup;
  uploader: FileUploader = new FileUploader({ isHTML5: true });

  @Input() dataset: string;
  formImport: FormGroup;

  constructor(
    private fb: FormBuilder,
    private importService: ImportService,
    private activityService: ActivityService,
  ) { }

  ngOnInit() {
    this.importForm = this.fb.group({
      subyekData: new FormControl(null, Validators.required),
      dataSource: new FormControl(null, Validators.required),
      excelUpload: new FormControl(null, Validators.required),
    });

    this.importService.getSysParamSubyekData('subyekdata').subscribe(
      result => {
        this.subyekData = result;
      },
    );

    this.importService.getSysParamSourceData('database').subscribe(
      result => {
        this.dataSource = result;
      },
    );

  }

  import() {
    if (this.validate()) {
      this.onProcess = true;
      for (let i = 0; i < this.uploader.queue.length; i++) {
        const fileItem = this.uploader.queue[i]._file;
        if (fileItem.size > 100000000) {
          this.onProcess = false;
          return;
        }
      }
      // upload the file
      for (let j = 0; j < this.uploader.queue.length; j++) {
        const data = new FormData();
        const fileItem = this.uploader.queue[j]._file;
        data.append('file', fileItem);
        data.append('dataset', this.dataset);
        data.append('subyek', this.importForm.controls.subyekData.value);
        data.append('sourcedata', this.importForm.controls.dataSource.value);
        this.importService.import(data).subscribe(
          result => {
            if (200 === result.status) {
              this.countSuccess++;
            }
            if (this.countSuccess === this.uploader.queue.length) {
              this.uploadSuccess();
            }
          },
          error => {
            this.onProcess = false;
            this.errors[0] = '[' + error.status + '] Import gagal. Pastikan data Anda sesuai!';
          },
        );
      }

      setTimeout(() => {
        if (this.successShow) {
          return this.successShow = false;
        }
      }, this.delayTime);
    }
  }

  uploadSuccess() {
    this.uploader.clearQueue();
    this.onProcess = false;
    this.successShow = true;
    this.activityService.saveLog('import', this.dataset, this.importForm.controls.subyekData.value,
            this.importForm.controls.dataSource.value, null, null).subscribe(
              response => {
                this.successShow = true;
              },
              error => {
                this.successShow = false;
                this.errors.push('[' + error.status + '] Import gagal. Kesalahan sistem!');
              },
            );
  }

  validate(): boolean {
    if (this.uploader.queue.length > 1) {
      alert('Hanya boleh upload 1 file');
      return false;
    }
    return true;
  }
}
