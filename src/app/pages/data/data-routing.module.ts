import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DataComponent } from './data.component';
import { InquiryComponent } from './inquiry/inquiry.component';
import { DatasetOptionComponent } from './dataset-option/dataset-option.component';

const routes: Routes = [{
  path: '',
  component: DataComponent,
  children: [
    {
      path: 'inquiry',
      component: InquiryComponent,
    },
    {
      path: 'importupload',
      component: DatasetOptionComponent,
    },
  ],
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [
    RouterModule,
  ],
})
export class DataRoutingModule { }
