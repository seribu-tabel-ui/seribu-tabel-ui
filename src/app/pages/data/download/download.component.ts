import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { MatTable } from '@angular/material';
import { UploadService } from '../../../shared/services/upload.service';
import { DownloadResponse } from '../../../models/download-response';
import * as fileSaver from 'file-saver';
import { Lov } from '../../../models/lov';
import { ActivityService } from '../../../shared/services/activity.service';

@Component({
  selector: 'ngx-download',
  templateUrl: './download.component.html',
  styleUrls: ['./download.component.scss'],
})
export class DownloadComponent implements OnInit {

  @Input() dataset: string;
  @ViewChild(MatTable, { static: true }) table: MatTable<any>;
  displayedColumns: string[] = ['action', 'subditName', 'releaseYear', 'fileName', 'description'];
  listFiles: DownloadResponse[];
  successShow: boolean = false;
  errors: string[] = [];
  delayTime = 4500;
  dataOwner: Lov[] = [];

  constructor(
    private uploadService: UploadService,
    private activityService: ActivityService,
  ) { }

  ngOnInit() {
    this.uploadService.getAllFiles(this.dataset).subscribe(
      result => {
        this.listFiles = result.result;
      },
      error => {
        this.listFiles = [];
      },
    );

    this.uploadService.getSysParamOwner(this.dataset).subscribe(
      result => {
        this.dataOwner = result;
      },
    );
  }

  download(obj) {
    for (let index = 0; index < this.dataOwner.length; index++) {
      const element = this.dataOwner[index];
      if (obj.owner === element.text) {
        obj.owner = element.value;
        break;
      }
    }

    this.uploadService.download(obj).subscribe(
      response => {
        const fileType = response.headers.get('content-type');
        this.saveFile(response.body, obj.owner, obj.fileName, fileType);
      },
      error => {
        this.successShow = false;
        this.errors[0] = 'file gagal diunduh';
      },
    );
  }

  saveFile(data: any,  ownerName: string, filename?: string, fileType?: string) {
    const blob = new Blob([data], { type: fileType });
    fileSaver.saveAs(blob, filename);
    this.successShow = true;
    this.activityService.saveLog('download', this.dataset, ownerName,
            null, null, null).subscribe(
              response => {
                this.successShow = true;
              },
              error => {
                this.successShow = false;
                this.errors.push('[' + error.status + '] File gagal diunduh. Kesalahan sistem!');
              },
            );
    setTimeout(() => {
      if (this.successShow) {
        return this.successShow = false;
      }
    }, this.delayTime);
  }

}
