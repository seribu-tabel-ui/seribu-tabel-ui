import { Component, OnInit } from '@angular/core';
import { Lov } from '../../../models/lov';
import { UploadService } from '../../../shared/services/upload.service';

@Component({
  selector: 'ngx-dataset-option',
  templateUrl: './dataset-option.component.html',
  styleUrls: ['./dataset-option.component.scss'],
})
export class DatasetOptionComponent implements OnInit {

  datasets: Lov[];
  dataset: string = '';

  constructor(
    private uploadService: UploadService,
  ) { }

  ngOnInit() {
    this.uploadService.getSysParamDataset('dataset').subscribe(
      (result: Lov[]) => {
        this.datasets = result;
      },
    );
  }

}
