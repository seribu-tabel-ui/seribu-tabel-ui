import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DatasetOptionComponent } from './dataset-option.component';

describe('DatasetOptionComponent', () => {
  let component: DatasetOptionComponent;
  let fixture: ComponentFixture<DatasetOptionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DatasetOptionComponent],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DatasetOptionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
