import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { Lov } from '../../../models/lov';
import { ImportService } from '../../../shared/services/import.service';
import { WizardRequest } from '../../../models/wizard-request';
import * as fileSaver from 'file-saver';
import { DatePipe } from '../../../@theme/pipes/date.pipe';
import { ActivityService } from '../../../shared/services/activity.service';

@Component({
  selector: 'ngx-wizard',
  templateUrl: './wizard.component.html',
  styleUrls: ['./wizard.component.scss'],
})
export class WizardComponent implements OnInit {

  @Input() dataset: string;
  wizardForm: FormGroup;
  progress: number = 0;
  subyekData: Lov[] = [];
  dataSource: Lov[] = [];
  countSuccess = 0;
  redirectDelay = 0;
  onProcess: boolean = false;
  errors: String[] = [];
  successShow: boolean = false;
  delayTime = 4500;
  listOfYears: number[] = [];
  yearChecked: string[] = [];
  listOfColumnHeader: Lov[] = [];
  requestData: WizardRequest;

  constructor(
    private importService: ImportService,
    private fb: FormBuilder,
    private pipeDate: DatePipe,
    private activityService: ActivityService,
  ) { }

  ngOnInit() {
    this.wizardForm = this.fb.group({
      subyekData: new FormControl(null, Validators.required),
      dataSource: new FormControl(null, Validators.required),
      rowParam: new FormControl(null, Validators.required),
      colParam: new FormControl(null, Validators.required),
    });

    this.importService.getSysParamSubyekData('subyekdata').subscribe(
      result => {
        this.subyekData = result;
      },
    );

    this.getListYear();
  }

  loadSourceData(selectedSubyek) {
    this.importService.getSysParamSourceData('database_'.concat(selectedSubyek)).subscribe(
      result => {
        this.dataSource = result;
      },
    );
  }

  dataSourceFilled() {
    const subyekdata = this.wizardForm.controls.subyekData.value;
    const datasource = this.wizardForm.controls.dataSource.value;
    this.importService.getLovCharacter(subyekdata, datasource).subscribe(
      result => {
        this.listOfColumnHeader = result;
      },
    );
  }

  exportData() {
    this.requestData = new WizardRequest(
      this.dataset,
      this.wizardForm.controls.subyekData.value,
      this.wizardForm.controls.dataSource.value,
      this.wizardForm.controls.rowParam.value,
      this.wizardForm.controls.colParam.value,
      this.yearChecked);
    this.importService.generateWizard(this.requestData).subscribe(
      response => {
        const fileType = response.headers.get('content-type');
        // console.log('response', response);
        this.saveFile(response.body,
          this.setFilename(this.requestData.dataset, this.requestData.subyekData, this.requestData.sourceData),
          fileType);
      },
    );
    this.activityService.saveLog('wizard', this.dataset,
      this.wizardForm.controls.subyekData.value,
      this.wizardForm.controls.dataSource.value,
      this.wizardForm.controls.rowParam.value, this.wizardForm.controls.colParam.value)
      .subscribe(
        response => {
          this.successShow = true;
        },
        error => {
          this.onProcess = false;
          this.errors[0] = '[' + error.status + '] Upload gagal. Kesalahan sistem!';
        },
      );
  }

  setFilename(dataset: string, subyekData: string, dataSource: string): string {
    return dataset.concat('_').concat(subyekData).concat('_').concat(dataSource).concat('_')
      .concat(this.generateDate()).concat('.xlsx');
  }

  generateDate(): string {
    return this.pipeDate.transform(new Date());
  }

  getListYear(): void {
    const year = new Date().getFullYear();
    for (let index = 0; index < 10; index++) {
      this.listOfYears.push(year - index);
    }
  }

  checkedYear(checked) {
    this.yearChecked.push(checked);
  }

  saveFile(data: any, filename?: string, fileType?: string) {
    const blob = new Blob([data], { type: fileType });
    fileSaver.saveAs(blob, filename);
    this.successShow = true;
    setTimeout(() => {
      if (this.successShow) {
        return this.successShow = false;
      }
    }, this.delayTime);
  }

}
