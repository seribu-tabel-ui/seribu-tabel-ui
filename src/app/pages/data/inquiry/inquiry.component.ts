import { Component, OnInit } from '@angular/core';
import { Lov } from '../../../models/lov';
import { UploadService } from '../../../shared/services/upload.service';

@Component({
  selector: 'ngx-inquiry',
  templateUrl: './inquiry.component.html',
  styleUrls: ['./inquiry.component.scss'],
})
export class InquiryComponent implements OnInit {

  datasets: Lov[] = [];
  dataset: string = '';

  constructor(
    private uploadService: UploadService,
  ) { }

  ngOnInit() {
    this.uploadService.getSysParamDataset('dataset').subscribe(
      (result: Lov[]) => {
        this.datasets = result;
      },
    );
  }

  changeDataset(dataset) {
    this.dataset = dataset;
  }
}
