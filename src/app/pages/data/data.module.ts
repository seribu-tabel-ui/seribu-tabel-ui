import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ThemeModule } from '../../@theme/theme.module';
import {
  NbAccordionModule,
  NbSelectModule,
  NbButtonModule,
  NbInputModule,
  NbCardModule,
  NbIconModule,
  NbProgressBarModule,
  NbTooltipModule,
  NbAlertModule,
  NbSpinnerModule,
  NbActionsModule,
  NbCheckboxModule,
} from '@nebular/theme';
import { NgxPaginationModule } from 'ngx-pagination';

import { DataRoutingModule } from './data-routing.module';
import { DataComponent } from './data.component';
import { InquiryComponent } from './inquiry/inquiry.component';
import { UploadComponent } from './upload/upload.component';
import { DatasetOptionComponent } from './dataset-option/dataset-option.component';
import { ImportComponent } from './import/import.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FileUploadModule } from 'ng2-file-upload';
import { UploadService } from '../../shared/services/upload.service';
import { CookieService } from 'ngx-cookie';
import { WizardComponent } from './wizard/wizard.component';
import { DownloadComponent } from './download/download.component';
import { MatTableModule } from '@angular/material';
import { ImportService } from '../../shared/services/import.service';
import { ActivityService } from '../../shared/services/activity.service';
import { DatePipe } from '../../@theme/pipes/date.pipe';


@NgModule({
  declarations: [DataComponent,
    InquiryComponent,
    UploadComponent,
    DatasetOptionComponent,
    ImportComponent,
    WizardComponent,
    DownloadComponent],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    DataRoutingModule,
    NgxPaginationModule,
    NbAccordionModule,
    ThemeModule,
    NbSelectModule,
    NbButtonModule,
    NbCardModule,
    NbIconModule,
    NbInputModule,
    NbProgressBarModule,
    NbTooltipModule,
    NbAlertModule,
    NbSpinnerModule,
    NbActionsModule,
    MatTableModule,
    FileUploadModule,
    NbCheckboxModule,
  ],
  providers: [
    UploadService,
    ActivityService,
    CookieService,
    ImportService,
    DatePipe,
  ],
})
export class DataModule { }
