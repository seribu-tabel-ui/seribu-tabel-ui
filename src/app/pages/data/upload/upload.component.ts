import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { FileUploader } from 'ng2-file-upload';
import { Lov } from '../../../models/lov';
import { UploadService } from '../../../shared/services/upload.service';
import { ActivityService } from '../../../shared/services/activity.service';

@Component({
  selector: 'ngx-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.scss'],
})
export class UploadComponent implements OnInit {

  @Input() dataset: string;
  uploadForm: FormGroup;
  uploader: FileUploader = new FileUploader({ isHTML5: true });
  file: any;
  progress: number = 0;
  dataOwner: Lov[] = [];
  countSuccess = 0;
  redirectDelay = 0;
  onProcess: boolean = false;
  errors: String[] = [];
  successShow: boolean = false;
  delayTime = 4500;
  listOfYears: number[] = [];

  constructor(
    private fb: FormBuilder,
    private uploadService: UploadService,
    private activityService: ActivityService,
  ) { }

  ngOnInit() {
    this.uploadForm = this.fb.group({
      ownerName: new FormControl(null, Validators.required),
      releaseYear: new FormControl(null, Validators.required),
      keterangan: new FormControl(null, Validators.required),
      docUpload: new FormControl(null, Validators.required),
    });

    this.uploadService.getSysParamOwner(this.dataset).subscribe(
      result => {
        this.dataOwner = result;
      },
    );

    this.getListYear();
  }

  upload() {
    if (this.validate()) {
      this.onProcess = true;
      for (let i = 0; i < this.uploader.queue.length; i++) {
        const fileItem = this.uploader.queue[i]._file;
        if (fileItem.size > 100000000) {
          this.onProcess = false;
          return;
        }
      }
      // upload the file
      for (let j = 0; j < this.uploader.queue.length; j++) {
        const data = new FormData();
        const fileItem = this.uploader.queue[j]._file;
        data.append('file', fileItem);
        data.append('dataset', this.dataset);
        data.append('owner', this.uploadForm.controls.ownerName.value);
        data.append('releaseYear', this.uploadForm.controls.releaseYear.value);
        data.append('description', this.uploadForm.controls.keterangan.value);
        this.uploadService.upload(data).subscribe(
          result => {
            if (200 === result.status) {
              this.countSuccess++;
            }
            if (this.countSuccess === this.uploader.queue.length) {
              this.uploadSuccess();
            }
          },
          error => {
            this.onProcess = false;
            this.errors[0] = '[' + error.status + '] Upload gagal. Pastikan data Anda sesuai!';
          },
        );
      }

      setTimeout(() => {
        if (this.successShow) {
          return this.successShow = false;
        }
      }, this.delayTime);
    }
  }

  uploadSuccess() {
    this.uploader.clearQueue();
    this.onProcess = false;
    this.successShow = true;
    this.activityService.saveLog('upload', this.dataset, this.uploadForm.controls.ownerName.value, null, null, null)
    .subscribe(
      response => {
        this.successShow = true;
      },
      error => {
        this.onProcess = false;
        this.errors[0] = '[' + error.status + '] Upload gagal. Kesalahan sistem!';
      },
    );
  }

  validate(): boolean {
    if (this.uploader.queue.length > 1) {
      alert('Hanya boleh upload 1 file');
      return false;
    }
    return true;
  }

  getListYear(): void {
    const year = new Date().getFullYear();
    for (let index = 0; index < 10; index++) {
      this.listOfYears.push(year - index);
    }
  }

  getIpAddress() {

  }
}
