import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WindowRegistrationComponent } from './window-registration.component';

describe('WindowRegistrationComponent', () => {
  let component: WindowRegistrationComponent;
  let fixture: ComponentFixture<WindowRegistrationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WindowRegistrationComponent ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WindowRegistrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
