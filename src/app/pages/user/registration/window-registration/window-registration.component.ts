import { Component, Inject, Optional } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { getDeepFromObject, NB_AUTH_OPTIONS } from '@nebular/auth';
import { CommonHelper } from '../../../../helper/common-helper';
import { Lov } from '../../../../models/lov';
import { UserRequest } from '../../../../models/user-request';
import { UserService } from '../../../../shared/services/user.service';

@Component({
  selector: 'ngx-window-registration',
  templateUrl: './window-registration.component.html',
  styleUrls: ['./window-registration.component.scss'],
})
export class WindowRegistrationComponent {

  action: string;
  local_data: any;
  public lovRoles: Lov[];
  showMessages: any = {};
  errors: string[] = [];
  redirectDelay: number = 0;
  disabled: boolean = false;
  selectedRole: Lov;

  constructor(
    public dialogRef: MatDialogRef<WindowRegistrationComponent>,
    public userService: UserService,
    public helper: CommonHelper,
    @Inject(NB_AUTH_OPTIONS) protected options = {},
    // @Optional() is used to prevent error if no data is passed
    @Optional() @Inject(MAT_DIALOG_DATA) public data: UserRequest) {
    this.local_data = { ...data };
    this.action = this.local_data.action;
    if (this.local_data.action === 'Ganti Password') {
      this.local_data.password = '';
    }
    this.showMessages = this.getConfigValue('forms.login.showMessages');
    this.userService.getRoles().subscribe(
      dataRole => {
        this.lovRoles = dataRole;
        this.generateRoleLov(this.local_data.roleText);
      });
  }

  generateRoleLov(roleText) {
    this.lovRoles.map(role => {
      if (roleText === role.text) {
        // this.selectedRole = role;
        this.selectedRole = new Lov(this.local_data.roleText, this.local_data.role);
      }
    });
  }

  generateRoleText(roleValue) {
    this.lovRoles.map(role => {
      if (roleValue === role.value) {
        this.local_data.roleText = role.text;
      }
    });
  }

  doAction() {
    this.errors = [];
    this.disabled = true;
    this.generateRoleText(this.local_data.role);
    if (this.validate()) {
      // console.log('data save: ', this.local_data);
      this.saveData(this.action);
    } else {
      this.errors[0] = 'Cek kembali data Anda!';
      this.disabled = false;
    }
  }

  validate(): boolean {
    if (((this.local_data.action === 'Tambah' || this.local_data.action === 'Ganti Password')
      && this.local_data.password === this.local_data.rePassword)
      && (this.local_data.nip.length === 9)
      && this.helper.hasProp(this.local_data, this.propAction(this.action))) {
      return true;
    } else if (this.local_data.action === 'Ubah' || this.local_data.action === 'Hapus') {
      return true;
    }
    return false;
  }

  propAction(action): string[] {
    let prop: string[];
    switch (action) {
      case 'Tambah':
        prop = ['name', 'username', 'nip', 'password', 'role', 'rePassword'];
        break;
      case 'Ubah':
        prop = ['name', 'username', 'nip', 'role'];
        break;
      case 'Ganti Password':
        prop = ['password', 'rePassword'];
        break;
    }
    return prop;
  }

  saveData(event: string) {
    switch (event) {
      case 'Tambah':
        this.userService.signup(this.generateUserRequestDto(this.local_data, true)).subscribe(
          result => {
            const state = result.status;
            if (state === 200) {
              this.successSave();
            }
          },
          error => {
            this.failure();
          });
        break;
      case 'Ubah':
      case 'Ganti Password':
        this.userService.updateUser(this.generateUserRequestDto(this.local_data, true)).subscribe(
          result => {
            const state = result.status;
            if (state === 200) {
              this.successSave();
            }
          },
          error => {
            this.failure();
          });
        break;
      case 'Hapus':
        this.userService.deleteUser(this.generateUserRequestDto(this.local_data, false)).subscribe(
          result => {
            const state = result.status;
            if (state === 200) {
              this.successSave();
            }
          },
          error => {
            this.failure();
          });
        break;
    }
  }

  successSave() {
    setTimeout(() => {
      return this.dialogRef.close({ event: this.action, data: this.local_data });
    }, this.redirectDelay);
    this.disabled = false;
  }

  failure() {
    this.errors[0] = 'Gagal simpan, mohon cek data Anda kembali.';
    this.disabled = false;
  }

  closeDialog() {
    this.dialogRef.close({ event: 'Batal/*  */' });
  }

  generateUserRequestDto(data: any, update: boolean): UserRequest {
    return new UserRequest(data.username, data.name, data.nip, data.password, data.role,
      data.roleText, update, data.createdDate);
  }

  getConfigValue(key: string): any {
    return getDeepFromObject(this.options, key, null);
  }
}
