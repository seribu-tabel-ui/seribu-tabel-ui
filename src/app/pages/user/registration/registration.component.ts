import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatTable } from '@angular/material';
import { WindowRegistrationComponent } from './window-registration/window-registration.component';
import { UserService } from '../../../shared/services/user.service';
import { UserRequest } from '../../../models/user-request';
import { Subscription } from 'rxjs';
import { Lov } from '../../../models/lov';

@Component({
  selector: 'ngx-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss'],
})
export class RegistrationComponent implements OnInit {

  delayTime = 4500;
  userData: UserRequest[];
  role: string;
  @ViewChild(MatTable, { static: true }) table: MatTable<any>;
  displayedColumns: string[] = ['action', 'name', 'userId', 'nip', 'otoritas'];
  act: string;
  alertShow: boolean = false;
  subscribtion: Subscription;
  lovRoles: Lov[];
  roleText: string;

  constructor(
    private userService: UserService,
    private dialog: MatDialog,
  ) { }

  ngOnInit() {
    this.userService.getAllUser().subscribe(
      data => {
        this.userData = data.result;
      },
      error => {
        this.userData = [];
      });
  }

  openDialog(action, obj) {
    obj.action = action;
    const dialogRef = this.dialog.open(WindowRegistrationComponent, {
      width: '550px',
      data: obj,
    });

    dialogRef.afterClosed().subscribe(result => {
      switch (result.event) {
        case 'Tambah':
          this.act = result.event.toLowerCase();
          this.alertShow = true;
          this.addRowData(result.data);
          break;
        case 'Ubah':
          this.act = result.event.toLowerCase();
          this.alertShow = true;
          this.updateRowData(result.data);
          break;
        case 'Ganti Password':
          this.act = result.event.toLowerCase();
          this.alertShow = true;
          this.changePassword(result.data);
          break;
        case 'Hapus':
          this.alertShow = true;
          this.act = result.event.toLowerCase();
          this.deleteRowData(result.data);
          break;
      }
      setTimeout(() => {
        return this.alertShow = false;
      }, this.delayTime);
    });
  }

  addRowData(row_obj) {
    const status: boolean = true;
    this.userData.push({
      id: null,
      nip: row_obj.nip,
      username: row_obj.username,
      name: row_obj.name,
      password: row_obj.password,
      role: row_obj.role,
      roleText: row_obj.roleText,
      enabled: status,
      createdDate: row_obj.createdDate,
    });
    this.table.renderRows();
  }

  updateRowData(row_obj) {
    this.userData = this.userData.filter((value, key) => {
      if (value.nip === row_obj.nip) {
        value.name = row_obj.name;
        value.username = row_obj.username;
        value.role = row_obj.roleText;
        value.password = row_obj.password;
        value.enabled = true;
      }
      return true;
    });
  }

  changePassword(row_obj) {
    this.userData = this.userData.filter((value, key) => {
      if (value.nip === row_obj.nip) {
        value.password = row_obj.password;
        value.enabled = true;
      }
      return true;
    });
  }

  deleteRowData(row_obj) {
    this.userData = this.userData.filter((value, key) => {
      return value.nip !== row_obj.nip;
    });
  }
}
