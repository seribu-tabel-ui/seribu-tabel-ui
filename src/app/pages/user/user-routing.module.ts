import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UserComponent } from './user.component';
import { ActivityComponent } from './activity/activity.component';
import { RegistrationComponent } from './registration/registration.component';

const routes: Routes = [{
  path: '',
  component: UserComponent,
  children: [
    {
      path: 'activity',
      component: ActivityComponent,
    },
    {
      path: 'registration',
      component: RegistrationComponent,
    },
  ],
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [
    RouterModule,
  ],
})
export class UserRoutingModule { }
