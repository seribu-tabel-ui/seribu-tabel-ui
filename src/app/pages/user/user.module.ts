import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ThemeModule } from '../../@theme/theme.module';
import { CommonModule } from '@angular/common';
import { NbAccordionModule,
  NbSelectModule,
  NbButtonModule,
  NbCardModule,
  NbIconModule,
  NbInputModule,
  NbActionsModule,
  NbWindowModule,
  NbAlertModule,
  NbSpinnerModule,
  NbTooltipModule,
 } from '@nebular/theme';
 import {MatTableModule, MatDialogModule, MatFormFieldModule, MatInputModule, MatButtonModule} from '@angular/material';

import { UserRoutingModule } from './user-routing.module';
import { ActivityComponent } from './activity/activity.component';
import { UserComponent } from './user.component';
import { RegistrationComponent } from './registration/registration.component';
import { WindowRegistrationComponent } from './registration/window-registration/window-registration.component';
import { UserService } from '../../shared/services/user.service';
import { CookieService } from 'ngx-cookie';
import { DigitOnlyModule } from '@uiowa/digit-only';
import { CommonHelper } from '../../helper/common-helper';
import { UploadService } from '../../shared/services/upload.service';
import { ImportService } from '../../shared/services/import.service';

@NgModule({
  declarations: [ActivityComponent, UserComponent, RegistrationComponent, WindowRegistrationComponent],
  imports: [
    ThemeModule,
    FormsModule,
    CommonModule,
    NbAlertModule,
    NbAccordionModule,
    UserRoutingModule,
    NbSelectModule,
    NbButtonModule,
    NbCardModule,
    NbIconModule,
    NbInputModule,
    MatTableModule,
    MatDialogModule,
    NbActionsModule,
    NbWindowModule.forChild(),
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    DigitOnlyModule,
    NbSpinnerModule,
    NbTooltipModule,
  ],
  entryComponents: [WindowRegistrationComponent],
  providers: [
    CommonHelper,
    CookieService,
    UserService,
    UploadService,
    ImportService,
  ],
})
export class UserModule { }
