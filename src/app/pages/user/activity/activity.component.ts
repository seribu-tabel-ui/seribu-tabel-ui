import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivityRequest } from '../../../models/activity-request';
import { ActivityService } from '../../../shared/services/activity.service';
import { MatTable } from '@angular/material';
import { UploadService } from '../../../shared/services/upload.service';
import { Lov } from '../../../models/lov';
import { ImportService } from '../../../shared/services/import.service';

@Component({
  selector: 'ngx-activity',
  templateUrl: './activity.component.html',
  styleUrls: ['./activity.component.scss'],
})
export class ActivityComponent implements OnInit {



  @ViewChild(MatTable, { static: true }) table: MatTable<any>;
  displayedColumns: string[] = ['userId', 'nip', 'authority', 'ipAddress', 'activity'
    , 'dataset', 'subyekData', 'dataSource', 'firstChar', 'secondChar', 'accessTime'];
  activityLogs: ActivityRequest[];
  datasets: Lov[];
  subyekDatas: Lov[];

  dataset: string;
  subyekdata: string;
  nip: string;
  username: string;

  searchData: string = '';

  constructor(
    private activityService: ActivityService,
    private uploadService: UploadService,
    private importService: ImportService,
  ) {

  }

  ngOnInit(): void {
    this.getAllData();
    this.uploadService.getSysParamDataset('dataset').subscribe(
      (result: Lov[]) => {
        this.datasets = result;
      },
    );

    this.importService.getSysParamSubyekData('subyekdata').subscribe(
      result => {
        this.subyekDatas = result;
      },
    );
  }

  onSearch() {
    this.activityService.searchLog(this.setSearchString()).subscribe(
      result => {
        const response = result.result;
        this.activityLogs = response;
      },
      error => {
        this.activityLogs = [];
      },
    );
  }

  setSearchString(): string {
    if (this.username !== null && this.username !== '' && this.username !== undefined) {
      this.searchData = this.searchData.concat('username').concat(':').concat(this.username).concat(';');
    }
    if (this.nip !== null && this.nip !== '' && this.nip !== undefined) {
      this.searchData = this.searchData.concat('nip').concat(':').concat(this.nip).concat(';');
    }
    if (this.dataset !== null && this.dataset !== '' && this.dataset !== undefined) {
      this.searchData = this.searchData.concat('dataset').concat(':').concat(this.dataset).concat(';');
    }
    if (this.subyekdata !== null && this.subyekdata !== '' && this.subyekdata !== undefined) {
      this.searchData = this.searchData.concat('subyekData').concat(':').concat(this.subyekdata).concat(';');
    }
    return this.searchData;
  }

  getAllData() {
    this.activityService.getAllLog().subscribe(
      result => {
        const response = result.result;
        this.activityLogs = response;
      },
      error => {
        this.activityLogs = [];
      },
    );
  }

  onReset() {
    this.subyekdata = '';
    this.dataset = '';
    this.nip = '';
    this.username = '';
    this.searchData = '';
    this.getAllData();
  }
}
