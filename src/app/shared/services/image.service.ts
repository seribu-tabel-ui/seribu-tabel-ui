import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Observable } from 'rxjs';

@Injectable()
export class ImageService {

  constructor(
    private httpClient: HttpClient,
  ) {}

  getLoginImage(): Observable<any> {
    return this.httpClient.get(environment.static_url + '/assets/data/images.json', {responseType: 'json'});
  }
}
