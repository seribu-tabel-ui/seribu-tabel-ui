import { Injectable, NgZone } from '@angular/core';
import { CookiesService } from './cookies.service';
import { ActivityRequest } from '../../models/activity-request';
import { UserModel } from '../../models/user-model';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Observable } from 'rxjs';

declare global {
  interface Window {
    RTCPeerConnection: RTCPeerConnection;
    mozRTCPeerConnection: RTCPeerConnection;
    webkitRTCPeerConnection: RTCPeerConnection;
  }
}

@Injectable()
export class ActivityService {

  private actLogUrl = environment.backend_url + '/user/api/savelog';
  private allLogUrl = environment.backend_url + '/user/api/alllog';
  private searchLogUrl = environment.backend_url + '/user/api/searchlog';

  localIp: any;
  activityLog: ActivityRequest;
  userLogedIn: UserModel;


  private ipRegex = new RegExp(/([0-9]{1,3}(\.[0-9]{1,3}){3}|[a-f0-9]{1,4}(:[a-f0-9]{1,4}){7})/);

  constructor(
    private zone: NgZone,
    private cookiesService: CookiesService,
    private http: HttpClient,
    ) {
      this.determineLocalIp();
  }

  getAuthHeader(): HttpHeaders {
    const jwt = this.cookiesService.getCookie('jwt');
    let header: HttpHeaders = new HttpHeaders();
    header = header.append(environment.authorization, jwt);
    return header;
  }

  private determineLocalIp() {
    window.RTCPeerConnection = this.getRTCPeerConnection();

    const pc = new RTCPeerConnection({ iceServers: [] });
    pc.createDataChannel('');
    pc.createOffer().then(pc.setLocalDescription.bind(pc));

    pc.onicecandidate = (ice) => {
      this.zone.run(() => {
        if (!ice || !ice.candidate || !ice.candidate.candidate) {
          return;
        }

        this.localIp = this.ipRegex.exec(ice.candidate.candidate)[1];
        sessionStorage.setItem('LOCAL_IP', this.localIp);

        pc.onicecandidate = () => { };
        pc.close();
      });
    };
  }

  private getIpAddress(): string {
    return sessionStorage.getItem('LOCAL_IP');
  }

  saveLog(activity, dataset, subyekData, sourceData, firstCharacter, secondCharacter): Observable<any> {
    this.getUserLogingIn();
    this.activityLog = new ActivityRequest(
      this.userLogedIn.username,
      this.userLogedIn.nip,
      this.getAuth()[0].authority,
      activity,
      dataset,
      subyekData,
      sourceData,
      firstCharacter,
      secondCharacter,
      this.getIpAddress(),
      new Date(),
    );
    return this.http.post(this.actLogUrl, this.activityLog, { headers: this.getAuthHeader() });
  }

  searchLog(searchString: string): Observable<any> {
    const params = new HttpParams().set('search', searchString);
    return this.http.get(this.searchLogUrl, {params, headers: this.getAuthHeader()});
  }

  getAuth(): any[] {
    this.userLogedIn = this.cookiesService.getCookie('user');
    return this.userLogedIn.authorities;
  }

  getUserLogingIn(): void {
    this.userLogedIn = this.cookiesService.getCookie('user');
  }

  private getRTCPeerConnection() {
    return window.RTCPeerConnection ||
      window.mozRTCPeerConnection ||
      window.webkitRTCPeerConnection;
  }

  getAllLog(): Observable<any> {
    return this.http.get(this.allLogUrl, { headers: this.getAuthHeader() });
  }

}
