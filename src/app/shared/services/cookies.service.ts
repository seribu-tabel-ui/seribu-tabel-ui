import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie';

@Injectable()
export class CookiesService {

  constructor(private cookieService: CookieService) { }

  setCookie(key: string, value: any) {
    this.cookieService.put(key, value);
  }

  getCookie(key: string): any {
    return this.cookieService.getObject(key);
  }

  setLogoutCookie() {
    this.cookieService.removeAll();
  }
}
