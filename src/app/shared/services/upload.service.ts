import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import { CookiesService } from './cookies.service';
import { environment } from '../../../environments/environment';
import { DownloadRequest } from '../../models/download-request';
import { UserModel } from '../../models/user-model';
import { Lov } from '../../models/lov';

@Injectable()
export class UploadService {
  private uploadUrl = environment.backend_url + '/trx/up/api/upload/process';
  private downloadUrl = environment.backend_url + '/trx/up/api/download';
  private sysParamUrl = environment.backend_url + '/trx/up/api/getsysparam';
  private getAllFilesUrl = environment.backend_url + '/trx/up/api/allfiles';
  private getTotalSubditUrl = environment.backend_url + '/trx/up/api/total/subdit';
  private getTotalPihakLainUrl = environment.backend_url + '/trx/up/api/total/pihaklain';

  pathPrefix = '/';
  userLogedIn: UserModel;
  lovOwner = new Subject<Lov[]>();
  lovDataSet = new Subject<Lov[]>();

  constructor(
    private http: HttpClient,
    private cookiesService: CookiesService,
  ) { }

  getAuthHeader(contentType?: string): HttpHeaders {
    const jwt = this.cookiesService.getCookie('jwt');
    let header: HttpHeaders = new HttpHeaders();
    header = header.append(environment.authorization, jwt);
    if (contentType !== null) {
      header = header.append(environment.accept, contentType);
    }
    return header;
  }

  upload(data: FormData): Observable<any> {
    data.append('uploadedBy', this.getUserLogingIn());
    return this.http.post(this.uploadUrl, data, { headers: this.getAuthHeader(null) });
  }

  download(requestData: DownloadRequest): Observable<any> {
    const urlWithParam = this.downloadUrl
      + this.pathPrefix + requestData.dataset
      + this.pathPrefix + requestData.owner
      + this.pathPrefix + requestData.releaseYear
      + this.pathPrefix + requestData.fileName;
    return this.http.get(urlWithParam,
      {
        headers: this.getAuthHeader(requestData.fileType),
        observe: 'response',
        responseType: 'blob',
      });
  }

  getSysParamOwner(dataset: String): Observable<Lov[]> {
    const urlWithParam = this.sysParamUrl + this.pathPrefix + dataset;
    this.http.get(urlWithParam, { headers: this.getAuthHeader(null) }).subscribe(
      (result: any) => {
        const listOwner: Lov[] = result.result;
        this.lovOwner.next(listOwner);
      },
    );
    return this.lovOwner.asObservable();
  }

  getSysParamDataset(groupParam: String): Observable<Lov[]> {
    const urlWithParam = this.sysParamUrl + this.pathPrefix + groupParam;
    this.http.get(urlWithParam, { headers: this.getAuthHeader(null) }).subscribe(
      (result: any) => {
        const lovDataSet: Lov[] = result.result;
        this.lovDataSet.next(lovDataSet);
      },
    );
    return this.lovDataSet.asObservable();
  }

  getAllFiles(dataset: string): Observable<any> {
    const urlWithParam = this.getAllFilesUrl
      + this.pathPrefix
      + dataset;
    return this.http.get(urlWithParam, { headers: this.getAuthHeader(null) });
  }

  getUserLogingIn(): string {
    this.userLogedIn = this.cookiesService.getCookie('user');
    return this.userLogedIn.username;
  }

  getTotalSubdit(): Observable<any> {
    return this.http.get(this.getTotalSubditUrl, { headers: this.getAuthHeader(null) });
  }

  getTotalPihakLain(): Observable<any> {
    return this.http.get(this.getTotalPihakLainUrl, { headers: this.getAuthHeader(null) });
  }

}
