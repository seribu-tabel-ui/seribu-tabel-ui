import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { UserRequest } from '../../models/user-request';
import { Observable, Subject } from 'rxjs';
import { CookiesService } from './cookies.service';
import { environment } from '../../../environments/environment';
import { UserModel } from '../../models/user-model';
import { Lov } from '../../models/lov';

@Injectable()
export class UserService {

  private allUsersUrl = environment.backend_url + '/user/api/all';
  private allUsersOnlyUrl = environment.backend_url + '/user/api/userOnly';
  private userSignUpUrl = environment.backend_url + '/user/api/signup';
  private allRrolesUrl = environment.backend_url + '/user/api/roles';
  // private findByUsernameUrl = environment.backend_url + '/user/api/user/';
  private updateUrl = environment.backend_url + '/user/api/update';

  userLogedIn: UserModel;
  role: string;
  lovRoles = new Subject<Lov[]>();

  constructor(
    private http: HttpClient,
    private cookiesService: CookiesService,
  ) {
  }

  getAuthHeader(): HttpHeaders {
    const jwt = this.cookiesService.getCookie('jwt');
    let  header: HttpHeaders = new HttpHeaders();
    header = header.append(environment.authorization, jwt);
    header = header.append(environment.content_type, environment.json_content);
    return header;
  }

  signup(userRequest: UserRequest): Observable<any> {
    return this.http.post(this.userSignUpUrl, JSON.stringify(userRequest), { headers: this.getAuthHeader() });
  }

  getRoles(): Observable<Lov[]> {
    this.http.get(this.allRrolesUrl, { headers: this.getAuthHeader() }).subscribe(
      (result: any) => {
        const listRoles: Lov[] = result.result;
        if (this.getUserLogedInRole() === 'ROLE_SUPER') {
          const index: number = listRoles.findIndex(i => i.value === 'ROLE_SUPER');
          if (index !== -1) {
            listRoles.splice(index, 1);
          }
        } else if (this.getUserLogedInRole() === 'ROLE_ADMIN') {
          const index: number = listRoles.findIndex(i => i.value === 'ROLE_SUPER');
          if (index !== -1) {
            listRoles.splice(index, 1);
            const idx: number = listRoles.findIndex(i => i.value === 'ROLE_ADMIN');
            if (idx !== -1) {
              listRoles.splice(idx, 1);
            }
          }
        }
        this.lovRoles.next(listRoles);
      });
    return this.lovRoles.asObservable();
  }

  getAllUser(): Observable<any> {
    if (this.getUserLogedInRole() === 'ROLE_SUPER') {
      return this.http.get(this.allUsersUrl, { headers: this.getAuthHeader() });
    } else {
      return this.http.get(this.allUsersOnlyUrl, { headers: this.getAuthHeader() });
    }
  }

  updateUser(userRequest: UserRequest): Observable<any> {
    return this.http.post(this.updateUrl, userRequest, { headers: this.getAuthHeader() });
  }

  deleteUser(userRequest: UserRequest): Observable<any> {
    return this.http.post(this.updateUrl, userRequest, { headers: this.getAuthHeader() });
  }

  getUserLogedInRole(): string {
    this.userLogedIn = this.cookiesService.getCookie('user');
    this.role = this.userLogedIn.role;
    return this.role;
  }
}
