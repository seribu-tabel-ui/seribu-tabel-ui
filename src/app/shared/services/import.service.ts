import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import { environment } from '../../../environments/environment';
import { Lov } from '../../models/lov';
import { CookiesService } from './cookies.service';
import { UserModel } from '../../models/user-model';
import { WizardRequest } from '../../models/wizard-request';


@Injectable()
export class ImportService {

  private sysParamUrl = environment.backend_url + '/trx/im/api/getsysparam';
  private importUrl = environment.backend_url + '/trx/im/api/importfile';
  private headerColumnUrl = environment.backend_url + '/trx/im/api/header';
  private getTotalDemografiUrl = environment.backend_url + '/trx/im/api/total/demografi';
  private getTotalKepatuhanUrl = environment.backend_url + '/trx/im/api/total/kepatuhan';
  private getTotalPenerimaanUrl = environment.backend_url + '/trx/im/api/total/penerimaan';
  private postWizardUrl = environment.backend_url + '/trx/im/api/wizard';

  pathPrefix = '/';
  userLogedIn: UserModel;
  lovDataSet = new Subject<Lov[]>();
  lovSubyekData = new Subject<Lov[]>();
  lovSourceData = new Subject<Lov[]>();
  lovCharacter = new Subject<Lov[]>();

  constructor(
    private http: HttpClient,
    private cookiesService: CookiesService,
  ) { }

  getAuthHeader(contentType?: string): HttpHeaders {
    const jwt = this.cookiesService.getCookie('jwt');
    let header: HttpHeaders = new HttpHeaders();
    header = header.append(environment.authorization, jwt);
    if (contentType !== null) {
      header = header.append(environment.accept, contentType);
    }
    return header;
  }

  import(data: FormData): Observable<any> {
    data.append('uploadedBy', this.getUserLogingIn());
    return this.http.post(this.importUrl, data, { headers: this.getAuthHeader(null) });
  }

  getSysParamDataset(groupParam: String): Observable<Lov[]> {
    const urlWithParam = this.sysParamUrl + this.pathPrefix + groupParam;
    this.http.get(urlWithParam, { headers: this.getAuthHeader(null) }).subscribe(
      (result: any) => {
        const lovDataSet: Lov[] = result.result;
        this.lovDataSet.next(lovDataSet);
      },
    );
    return this.lovDataSet.asObservable();
  }

  getSysParamSubyekData(dataset: String): Observable<Lov[]> {
    const urlWithParam = this.sysParamUrl + this.pathPrefix + dataset;
    this.http.get(urlWithParam, { headers: this.getAuthHeader(null) }).subscribe(
      (result: any) => {
        const listSubyekData: Lov[] = result.result;
        this.lovSubyekData.next(listSubyekData);
      },
    );
    return this.lovSubyekData.asObservable();
  }

  getSysParamSourceData(groupParam: String): Observable<Lov[]> {
    const urlWithParam = this.sysParamUrl + this.pathPrefix + groupParam;
    this.http.get(urlWithParam, { headers: this.getAuthHeader(null) }).subscribe(
      (result: any) => {
        const lovSourceData: Lov[] = result.result;
        this.lovSourceData.next(lovSourceData);
      },
    );
    return this.lovSourceData.asObservable();
  }

  getUserLogingIn(): string {
    this.userLogedIn = this.cookiesService.getCookie('user');
    return this.userLogedIn.username;
  }

  getLovCharacter(subyekdata: string, datasource: string): Observable<Lov[]> {
    const urlWithParam = this.headerColumnUrl + this.pathPrefix + subyekdata + this.pathPrefix + datasource;
    this.http.get(urlWithParam, { headers: this.getAuthHeader(null) }).subscribe(
      (result: any) => {
        const lov: Lov[] = result.result;
        this.lovCharacter.next(lov);
      },
    );
    return this.lovCharacter.asObservable();
  }

  getTotalDemografi(): Observable<any> {
    return this.http.get(this.getTotalDemografiUrl, { headers: this.getAuthHeader(null) });
  }

  getTotalKepatuhan(): Observable<any> {
    return this.http.get(this.getTotalKepatuhanUrl, { headers: this.getAuthHeader(null) });
  }

  getTotalPenerimaan(): Observable<any> {
    return this.http.get(this.getTotalPenerimaanUrl, { headers: this.getAuthHeader(null) });
  }

  generateWizard(requestData: WizardRequest): Observable<any> {
    return this.http.post(this.postWizardUrl, requestData,
      {
        headers: this.getAuthHeader(null),
        observe: 'response',
        responseType: 'blob',
      });
  }
}
