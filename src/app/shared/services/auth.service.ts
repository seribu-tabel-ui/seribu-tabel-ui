import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { LoginRequest } from '../../models/login-request';
import { Observable } from 'rxjs';
import { CookiesService } from './cookies.service';
import { environment } from '../../../environments/environment';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root',
})
export class AuthService {

  private authUrl = environment.backend_url + '/auth/login';
  errMsg: string;
  constructor(
    private http: HttpClient,
    private cookiesService: CookiesService,
    private router: Router,
  ) {
    this.errMsg = '';
  }

  authenticate(loginRequest: LoginRequest): Observable<any> {
    return this.http.post(this.authUrl, JSON.stringify(loginRequest), {observe: 'response'});
  }

  errorLogout(message: any) {
    this.cookiesService.setLogoutCookie();
    this.errMsg = message;
    this.redirectToRoute('login');
  }

  redirectToRoute(route: string) {
    this.router.navigate([route]);
  }

}
